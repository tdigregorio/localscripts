var localScripts = function()
{
	// Local scripts evaluation
	var scripts = document.getElementsByTagName('script');
	for(var i = 0, script; script = scripts[i++];)
		if(script.type == 'text/javascript/local' && script.parentNode && !script.executed)
			try {
				eval("(function(){" + script.innerHTML + "})").call(script.parentNode);
				script.executed = true;
			} catch(e) {}
};
// Local scripts needs content to be accessible
if(document.attachEvent)
	document.attachEvent('DOMContentLoaded', localScripts);
else
	document.addEventListener('DOMContentLoaded', localScripts);