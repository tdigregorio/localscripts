localScript Javascript helper - Thomas Di Grégorio - https://bitbucket.org/tdigregorio/localScripts/src

# localScripts.js concept

Local scripts are javascript inline script tags with a type defined as text/javascript/local and are executed in the context of parent node.
Simply clever shortcut for extending nodes without having to select it.


Let's imagine your start with this HTML file embedding localScripts.js:

	<!doctype html>
	<html>
		<head>
			<title>My Webapp</title>
			<script type="text/javascript" src="localScripts.js"></script>
		</head>
		<body>
			<div class="Page">
				<h1>Hello World</h1>
			</div>
		</body>
	</html>

Now let's add a local script to the div node:

	<body>
		<div class="Page">
			<script type="text/javascript/local">
			this.title = this.getElementByTagName('h1')[0];
			</script>
			<h1>Hello World</h1>
		</div>
	</body>
	
Now the div node object gain a title property pointing to the h1 node.
The `this` keyword refer to the script's parent node, here the div.

The browser will in fact ignore the script because of type attribute different from normal `text/javascript` value.
localScripts.js will transform this code into a function and evaluate it with the parentNode context.


# A more complex exemple

Adding properties and methods to a particular node is simplified by adding the code directly into the node:

	<div class="Gallery">
		<script type="text/javascript/local">
		// var definition are private to the local script's generated function scope
		var _currentPic = 0,
		    _pics = Array.prototype.slice.call(this.getElementsByTagName('img')),
			_hideImg = function(img) {
				img.style.display = 'none';
			};
		
		// Hide all <img>s
		_pics.forEach(_hideImg);
		
		// Define methods
		this.show = function(id) {
			if(id > 0 && id < _pics.length)
			{
				_pics.forEach(_hideImg);
				_pics[id].style.display = "block";
			}
		};
		this.next = function() {
			_pics.forEach(_hideImg);
			_currentPic = _currentPic + 1 >= _pics.length ? 0 : _currentPic + 1;
			this.show(_currentPic);
		};
		this.prev = function() {
			_pics.forEach(_hideImg);
			_currentPic = _currentPic - 1 < 0 ? _pics.length - 1 : _currentPic - 1;
			this.show(_currentPic);
		};
		</script>
		
		<img src="pictures/picture1.jpg"/>
		<img src="pictures/picture2.jpg"/>
		<img src="pictures/picture3.jpg"/>
		<img src="pictures/picture4.jpg"/>
		<img src="pictures/picture5.jpg"/>
	</div>

the _currentPic, _pics and _hideImg var are private because declared in the context of the on-the-fly function generated from local script. But not saved anywhere.
But public methods, spread to the world by setting a property to `this` keyword, still references these variable, so garbage collector won't delete it.

You can now add 2 button into the div.Gallery to interact with it:

	<button onClick="parentNode.next()">NEXT</button>
	<button onClick="parentNode.prev()">PREV</button>


# Adding some localScripts later

You can search and execute every `<script type="text/javascript/local">` by calling the global function `localScripts()` (no arguments).
This will skip already executed local scripts, but executes the new ones added in the DOM in response of an AJAX call ...

# Re-execute localScripts

Every local scripts are executed in a `try` statement so no error are thrown.
When the script is executed with success the script node gain a `executed` boolean property.
If true, a subsequent call of `localScripts()` won't execute this local script.

You may need to re-execute some how the node's content after re-parenting it. To achieve this simply set `executed` flag to false and call `localScript()` again.








